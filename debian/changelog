rust-portable-atomic (1.10.0-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Tue, 11 Mar 2025 18:20:23 +0000

rust-portable-atomic (1.10.0-3) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.10.0 from crates.io using debcargo 2.7.5
  * Extend eliminate-test-helper.patch to support arm64.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 09 Dec 2024 01:05:16 +0000

rust-portable-atomic (1.10.0-2) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.10.0 from crates.io using debcargo 2.7.5
  * Relax dev-dependency on libc
  * Remove dev-dependency on windows-sys.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 08 Dec 2024 11:33:39 +0000

rust-portable-atomic (1.10.0-1) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.10.0 from crates.io using debcargo 2.7.5
  * Update patches for new upstream.
  * Adust gate-tests.patch to fix autopkgtest failure on s390x with rustc
    1.83

 -- Peter Michael Green <plugwash@debian.org>  Sun, 08 Dec 2024 01:31:57 +0000

rust-portable-atomic (1.9.0-4) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.9.0 from crates.io using debcargo 2.7.0
  * Further expand eliminate-test-helper.patch to cover more riscv code.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 30 Oct 2024 01:16:58 +0000

rust-portable-atomic (1.9.0-3) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.9.0 from crates.io using debcargo 2.7.0
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 19 Oct 2024 03:50:38 +0000

rust-portable-atomic (1.9.0-2) experimental; urgency=medium

  * Team upload.
  * Package portable-atomic 1.9.0 from crates.io using debcargo 2.7.0
  * Expand eliminate-test-helper.patch to cover riscv implementation.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 19 Oct 2024 03:33:55 +0000

rust-portable-atomic (1.9.0-1) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.9.0 from crates.io using debcargo 2.7.0
  * Update patches for new upstream.
  * Relax dev-dependency on crossbeam-utils.
  * Fix cfg directives in gate-tests.patch to use 'feature = "fallback"'
    instead of just 'fallback'
  * Mark tests for the newly added "force-amo" feature as broken.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 15 Oct 2024 23:29:02 +0000

rust-portable-atomic (1.4.3-2) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.4.3 from crates.io using debcargo 2.6.0
  * Remove unused patch resulting from typo.
  * Extend gate-tests.patch to support armel.
  * Extend eliminate-test-helper.patch to support arm64.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 28 Sep 2023 07:57:38 +0000

rust-portable-atomic (1.4.3-1) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 1.4.3 from crates.io using debcargo 2.6.0
  * Update gate-tests.patch for new upstream.
  * Add patch to eliminate test_helper subcrate, which is not included in the
    crates.io release.
  * Set test_is_broken for the unsafe-assume-single-core, disable-fiq, s-mode
    and critical-section features, they only make sense on "bare-metal" targets.
  * Eliminate dev-dependency on build-context crate, this makes the testsuite
    slightly less general, but it's still fine for our purposes.
  * Relax dev-dependency on fastrand to allow 1.8.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 24 Sep 2023 09:04:13 +0000

rust-portable-atomic (0.3.19-1+apertis1) apertis; urgency=medium

  * Add debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 13 Apr 2023 19:40:10 +0530

rust-portable-atomic (0.3.19-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 13 Apr 2023 14:56:00 +0530

rust-portable-atomic (0.3.19-1) unstable; urgency=medium

  * Team upload.
  * Package portable-atomic 0.3.19 from crates.io using debcargo 2.6.0
  * Update gate-tests.patch to support armel.
  * Update gate-tests.patch for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 28 Jan 2023 01:16:45 +0000

rust-portable-atomic (0.3.15-1) unstable; urgency=medium

  * Package portable-atomic 0.3.15 from crates.io using debcargo 2.5.0

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Sun, 30 Oct 2022 11:05:16 +0100
